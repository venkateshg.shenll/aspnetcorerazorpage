﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using AspNetCoreRazorPage.Models;

namespace AspNetCoreRazorPage.Data
{
    public class EmpContext : DbContext
    {
        public EmpContext (DbContextOptions<EmpContext> options)
            : base(options)
        {
        }

        public DbSet<AspNetCoreRazorPage.Models.EmpDetails> EmpDetails { get; set; }
    }
}

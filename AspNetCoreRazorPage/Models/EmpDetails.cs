﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace AspNetCoreRazorPage.Models
{
    public class EmpDetails
    {
        [Key]
        public int EmpId { get; set; }
        [Required]
        [DisplayName("Name")]
        public string EmpName { get; set; }
        [Required]
        [DataType(DataType.EmailAddress)]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "E-mail is not valid")]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        [MinLength(10, ErrorMessage = "Enter ten digit mobile number")]
        [MaxLength(10)]
        [DisplayName("Mobile Number")]
        public string Mobile { get; set; }
        [Required]
        public int Salary { get; set; }

    }
}

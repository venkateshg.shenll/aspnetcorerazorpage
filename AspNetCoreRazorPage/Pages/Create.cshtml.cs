﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using AspNetCoreRazorPage.Data;
using AspNetCoreRazorPage.Models;

namespace AspNetCoreRazorPage.Pages
{
    public class CreateModel : PageModel
    {
        private readonly AspNetCoreRazorPage.Data.EmpContext _context;

        public CreateModel(AspNetCoreRazorPage.Data.EmpContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public EmpDetails EmpDetails { get; set; }

        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.EmpDetails.Add(EmpDetails);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}

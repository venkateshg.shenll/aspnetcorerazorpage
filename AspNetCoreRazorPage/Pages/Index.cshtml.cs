﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using AspNetCoreRazorPage.Data;
using AspNetCoreRazorPage.Models;

namespace AspNetCoreRazorPage.Pages
{
    public class IndexModel : PageModel
    {
        private readonly AspNetCoreRazorPage.Data.EmpContext _context;

        public IndexModel(AspNetCoreRazorPage.Data.EmpContext context)
        {
            _context = context;
        }

        public IList<EmpDetails> EmpDetails { get;set; }

        public async Task OnGetAsync()
        {
            EmpDetails = await _context.EmpDetails.ToListAsync();
        }
    }
}
